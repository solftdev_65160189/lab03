package com.mycompany.lab03;

public class Lab03 {
    public static int add(int num1, int num2) {
        return num1 + num2;
    }
    
    public static boolean checkWin(String[] table) {
        if (checkRow(table) == true) {
            return true;
        }
        if (checkCol(table) == true) {
            return true;
        }
        if (checkRowCol(table) == true) {
            return true;
        }
        if (checkDraw(table) == true) {
            return true;
        }
        return false;
    }
    
    public static boolean checkRow(String[] table) {
    for (int row = 0; row < 3; row++) {
        int q = row * 3;
        if (table[q].equals("O")) {
            if (table[q + 1].equals("O")) {
                if (table[q + 2].equals("O")) {
                    return true;
                }
            }
        }
        if (table[q].equals("X")) {
            if (table[q + 1].equals("X")) {
                if (table[q + 2].equals("X")) {
                    return true;
                }
            }
        }
    }
    return false;
}

    
    
    public static boolean checkCol(String[] table) {
    for (int col = 0; col < 3; col++) {
        if (table[col].equals("O")) {
            if (table[col + 3].equals("O")) {
                if (table[col + 6].equals("O")) {
                    return true;
                }
            }
        }
        if (table[col].equals("X")) {
            if (table[col + 3].equals("X")) {
                if (table[col + 6].equals("X")) {
                    return true;
                }
            }
        }
    }
    return false;
}

    
    public static boolean checkRowCol(String[] table) {
    if (table[0].equals("O")) {
        if (table[4].equals("O")) {
            if (table[8].equals("O")) {
                return true;
            }
        }
    }
    if (table[0].equals("X")) {
        if (table[4].equals("X")) {
            if (table[8].equals("X")) {
                return true;
            }
        }
    }
    if (table[2].equals("O")) {
        if (table[4].equals("O")) {
            if (table[6].equals("O")) {
                return true;
            }
        }
    }
    if (table[2].equals("X")) {
        if (table[4].equals("X")) {
            if (table[6].equals("X")) {
                return true;
            }
        }
    }
    return false;
}


    public static boolean checkDraw(String[] table) {
    for (int i = 0; i < table.length; i++) {
        if (table[i].equals("O") || table[i].equals("X")) {
            return false;
        }
    }
    return true;
}




}
