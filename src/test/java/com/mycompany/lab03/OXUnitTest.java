/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author gamme
 */
/**
 *
 * @author asus
 */
public class oxUnitTest {
    
    public oxUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

   @Test
    public void TestCheckWin_O_Verticell_output_true() {
    String[] table = {"O", "2", "3", 
                            "O", "5", "6", 
                            "O", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}

    @Test
    public void TestCheckWin_O_Verticell2_output_true() {
    String[] table = {"1", "O", "3",
                            "4", "O", "6", 
                            "7", "O", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}

    @Test
    public void TestCheckWin_O_Verticell3_output_true() {
    String[] table = {"1", "2", "O", 
                            "4", "5", "O", 
                            "7", "8", "O"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}

    @Test
    public void TestCheckWin_O_Verticell_output_false() {
    String[] table = {"1", "2", "O", 
                            "4", "5", "O", 
                            "7", "O", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(false, result);
    }
    
    @Test
    public void TestCheckWin_O_Colcell_output_false() {
    String[] table = {"1", "O", "O", 
                            "O", "5", "6", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(false, result);
    }
    
    @Test
    public void TestCheckWin_O_Colcell1_output_true() {
    String[] table = {"O", "O", "O", 
                            "4", "5", "6", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_O_Colcell2_output_true() {
    String[] table = {"1", "2", "3", 
                            "O", "O", "O", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_O_Colcell3_output_true() {
    String[] table = {"1", "2", "3", 
                            "4", "5", "6", 
                            "O", "O", "O"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_O_ColVel_output_true() {
    String[] table = {"O", "2", "3", 
                            "4", "O", "6", 
                            "7", "8", "O"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_O_ColVel2_output_true() {
    String[] table = {"1", "2", "O", 
                            "4", "O", "6", 
                            "O", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_Verticell_output_false() {
    String[] table = {"1", "2", "X", 
                            "4", "5", "X", 
                            "7", "X", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(false, result);
    }
    
    @Test
    public void TestCheckWin_X_Colcell_output_false() {
    String[] table = {"1", "X", "X", 
                            "X", "5", "6", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(false, result);
    }
    @Test
    public void TestCheckWin_X_Verticell_output_true() {
    String[] table = {"X", "2", "3", 
                            "X", "5", "6", 
                            "X", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}

    @Test
    public void TestCheckWin_X_Verticell2_output_true() {
    String[] table = {"1", "X", "3",
                            "4", "X", "6", 
                            "7", "X", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}

    @Test
    public void TestCheckWin_X_Verticell3_output_true() {
    String[] table = {"1", "2", "X", 
                            "4", "5", "X", 
                            "7", "8", "X"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
}
    @Test
    public void TestCheckWin_X_Colcell1_output_true() {
    String[] table = {"X", "X", "X", 
                            "4", "5", "6", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_Colcell2_output_true() {
    String[] table = {"1", "2", "3", 
                            "X", "X", "X", 
                            "7", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_Colcell3_output_true() {
    String[] table = {"1", "2", "3", 
                            "4", "5", "6", 
                            "X", "X", "X"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_ColVerCell_output_true() {
    String[] table = {"1", "2", "X", 
                            "4", "X", "6", 
                            "X", "8", "9"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_ColVerCell2_output_true() {
    String[] table = {"X", "2", "3", 
                            "4", "X", "6", 
                            "7", "8", "X"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(true, result);
    }
    
    @Test
    public void TestCheckWin_X_Draw_output_true() {
    String[] table = {"X", "O", "O", 
                            "O", "X", "X", 
                            "X", "X", "O"};
    
    boolean result = Lab03.checkWin(table);
    assertEquals(false, result);
    }
}
